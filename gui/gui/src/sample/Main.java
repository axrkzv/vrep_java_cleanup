package sample;

import javafx.application.Application;
import coppelia.IntW;
import coppelia.IntWA;
import coppelia.FloatWA;
import coppelia.remoteApi;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Stream;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import vrepservice.VrepService;

import javafx.scene.Group;

public class Main extends Application {

    private ArrayList<Integer> handles = new ArrayList<Integer>();

    @Override
    public void start(Stage primaryStage) throws Exception{
       // Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("piton");
        primaryStage.setResizable(false);


       //////////////////////////////////plot
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("R");
        yAxis.setLabel("N");
        //creating the chart
        final LineChart<Number,Number> lineChart =
                new LineChart<Number,Number>(xAxis,yAxis);

        lineChart.setTitle("piton");
        //defining a series



        lineChart.setLayoutX(120);
        lineChart.setLayoutY(10);
        lineChart.setPrefSize(770, 590);
        ////////////////////////////////////////////////////
        Group root = new Group();

        //////////////////////////////////////////////////////buttons
        Button btnPlot = new Button();
        Button btnStart = new Button();
        Button btnCalc = new Button();
        Button btnTest = new Button();
        Label R = new Label("R:");
        Label minR = new Label("minR:");
        Label stepR = new Label("stepR:");
        Label warning = new Label("Enter a number!");
        warning.setTextFill(Color.web("red"));
        TextField textR = new TextField ();
        TextField textMinR = new TextField ();
        TextField textStepR = new TextField ();
        TextField percents = new TextField ();
        btnCalc.setDisable(true);
        //btnPlot.setDisable(true);

        btnTest.setLayoutX(10);
        btnTest.setLayoutY(250);
        btnTest.setPrefWidth(100);
        btnTest.setText("Test");
        btnTest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            }
        });


        btnPlot.setLayoutX(10);
        btnPlot.setLayoutY(90);
        btnPlot.setPrefWidth(100);
        btnPlot.setText("Clear Handles");
        btnPlot.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handles.clear();
            }
        });
        btnStart.setLayoutX(10);
        btnStart.setLayoutY(10);
        btnStart.setPrefWidth(100);
        btnStart.setText("Start");
        btnStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {




                        VrepService.vrep.simxFinish(-1);
                        int clientID = VrepService.vrep.simxStart("127.0.0.1", 19999, true, true, 5000, 5);
                        if (clientID == -1) {
                            System.out.println("Failed connecting to remote API server");
                            return;
                        }

                        ArrayList<Integer> newHandles = VrepService.GenerateGarbage(clientID, 100);

                        handles.addAll(newHandles);

                        warning.setVisible(false);
                        btnCalc.setDisable(false);


                }

        });
        btnCalc.setLayoutX(10);
        btnCalc.setLayoutY(50);
        btnCalc.setPrefWidth(100);
        btnCalc.setText("Calc");
        btnCalc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String rText = textR.getText();
                String minRText = textR.getText();
                String stepRText = textR.getText();
                if (rText.equals("")) {
                    warning.setVisible(true);

                } else {
                    try {
                        Float r = Float.parseFloat(rText);
                        Float mR = Float.parseFloat(minRText);
                        Float sR = Float.parseFloat(stepRText);
                        VrepService.vrep.simxFinish(-1);
                        int clientID = VrepService.vrep.simxStart("127.0.0.1", 19999, true, true, 5000, 5);
                        if (clientID == -1) {
                            System.out.println("Failed connecting to remote API server");
                            return;
                        }

                        double[][] xy = VrepService.CheckCleaningQuality(clientID, handles, mR, r, sR);

                        XYChart.Series series = getSeries(xy);
                        lineChart.getData().add(series);
                    } catch (NumberFormatException nfe) {
                        warning.setVisible(true);
                    }
                }
            }
        });

        R.setLayoutX(10);
        R.setLayoutY(130);
        minR.setLayoutX(10);
        minR.setLayoutY(170);
        stepR.setLayoutX(10);
        stepR.setLayoutY(210);

        warning.setLayoutX(10);
        warning.setLayoutY(160);
        warning.setVisible(false);

        textR.setLayoutX(30);
        textR.setLayoutY(130);
        textR.setPrefWidth(80);
        textMinR.setLayoutX(50);
        textMinR.setLayoutY(170);
        textMinR.setPrefWidth(60);
        textStepR.setLayoutX(50);
        textStepR.setLayoutY(210);
        textStepR.setPrefWidth(60);
        percents.setLayoutX(10);
        percents.setLayoutY(290);
        percents.setPrefWidth(100);

        root.getChildren().add(percents);
        root.getChildren().add(R);
        root.getChildren().add(textR);
        root.getChildren().add(warning);
        root.getChildren().add(textMinR);
        root.getChildren().add(textStepR);
        root.getChildren().add(minR);
        root.getChildren().add(stepR);
        //root.getChildren().add(textR);
        /////////////////////////////////////////////////////////////////////////
        root.getChildren().add(lineChart);
        root.getChildren().add(btnPlot);
        root.getChildren().add(btnStart);
        root.getChildren().add(btnCalc);
        root.getChildren().add(btnTest);
        Scene s = new Scene(root, 900, 600);

        primaryStage.setScene(s);
        primaryStage.show();


    }

    private  static XYChart.Series getSeries(double[][] xy)
    {

        XYChart.Series series = new XYChart.Series();
        series.setName("robopiton");
        for(int i=0;i<xy.length;i++) {
            series.getData().add(new XYChart.Data(xy[i][0], xy[i][1]));
        }

        return series;
    }

    public static void main(String[] args) {
        launch(args);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
}
