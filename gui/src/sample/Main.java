package sample;

import java.lang.Object;
import javafx.application.Application;
import coppelia.IntW;
import coppelia.IntWA;
import coppelia.FloatWA;
import coppelia.remoteApi;
import java.util.ArrayList;

import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import vrepservice.VrepService;

import javafx.scene.Group;

public class Main extends Application {

    private ArrayList<Integer> handles = new ArrayList<Integer>();
    private static double[][] matrix;
    private static int[] mask;
    public static double RESULT;
    @Override
    public void start(Stage primaryStage) throws Exception{
       // Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Plot");
        primaryStage.setResizable(false);


       //////////////////////////////////plot
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        xAxis.setLabel("R");
        yAxis.setLabel("N");
        //creating the chart
        final LineChart<Number,Number> lineChart =
                new LineChart<Number,Number>(xAxis,yAxis);

        lineChart.setTitle("Level cleaning");
        //defining a series



        lineChart.setLayoutX(220);
        lineChart.setLayoutY(10);
        lineChart.setPrefSize(770, 590);
        ////////////////////////////////////////////////////
        Group root = new Group();

        //////////////////////////////////////////////////////buttons
        Button btnPlot = new Button();
        Button btnStart = new Button();
        Button btnCalc = new Button();
        Button btnTest = new Button();
        Label R = new Label("R:");

        Label stepR = new Label("stepR:");
        Label warning = new Label("Enter a number!");
        Label labelStart = new Label("Garbage count:");
        Label labelResult = new Label("Result:");
        Label labelTest = new Label("% for clean:");
        warning.setTextFill(Color.web("red"));

        TextField textR = new TextField ();
        TextField textForStart = new TextField ();
        TextField textForResult = new TextField ();
        TextField textStepR = new TextField ();
        TextField percents = new TextField ();
        btnCalc.setDisable(true);
        btnTest.setDisable(true);
        //btnPlot.setDisable(true);

        btnTest.setLayoutX(110);
        btnTest.setLayoutY(330);
        btnTest.setPrefWidth(100);
        btnTest.setText("Test");
        btnTest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String percentOfCleaningStr = percents.getText();
                if (percentOfCleaningStr.equals("")) {
                    //warning.setVisible(true);
                } else {
                    try {
                        int percentOfCleaning = Integer.parseInt(percentOfCleaningStr);

                        if(percentOfCleaning < 1 || percentOfCleaning > 100)
                            return;

                        VrepService.vrep.simxFinish(-1);
                        int clientID = VrepService.vrep.simxStart("127.0.0.1", 19999, true, true, 5000, 5);
                        if (clientID == -1) {
                            System.out.println("Failed connecting to remote API server");
                            return;
                        }

                        VrepService.CleanUp(clientID, handles, percentOfCleaning);
                    }
                    finally {
                        VrepService.vrep.simxFinish(-1);
                    }
                }
            }
        });


        btnPlot.setLayoutX(110);
        btnPlot.setLayoutY(410);
        btnPlot.setPrefWidth(100);
        btnPlot.setText("Clear Handles");
        btnPlot.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handles.clear();
                btnCalc.setDisable(true);
                btnTest.setDisable(true);
            }
        });
        btnStart.setLayoutX(110);
        btnStart.setLayoutY(50);
        btnStart.setPrefWidth(100);
        btnStart.setText("Start");
        btnStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String gCountStr = textForStart.getText();
                if (gCountStr.equals("")) {
                    //warning.setVisible(true);
                } else {


                    try {
                        int gCount = Integer.parseInt(gCountStr);

                        if(gCount < 1)
                            return;

                        VrepService.vrep.simxFinish(-1);
                        int clientID = VrepService.vrep.simxStart("127.0.0.1", 19999, true, true, 5000, 5);
                        if (clientID == -1) {
                            System.out.println("Failed connecting to remote API server");
                            return;
                        }
                        //textForStart
                        //textForResult
                        ArrayList<Integer> newHandles = VrepService.GenerateGarbage(clientID, gCount);

                        handles.addAll(newHandles);

                        warning.setVisible(false);
                        btnCalc.setDisable(false);
                        btnTest.setDisable(false);
                    } finally {
                        VrepService.vrep.simxFinish(-1);
                    }
                }
            }
        });
        btnCalc.setLayoutX(110);
        btnCalc.setLayoutY(210);
        btnCalc.setPrefWidth(100);
        btnCalc.setText("Calc");
        btnCalc.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String rText = textR.getText();
                String stepRText = textStepR.getText();
                if (rText.equals("")) {
                    //warning.setVisible(true);

                } else {
                    try {
                        Float r = Float.parseFloat(rText);
                        Float sR = Float.parseFloat(stepRText);
                        VrepService.vrep.simxFinish(-1);
                        int clientID = VrepService.vrep.simxStart("127.0.0.1", 19999, true, true, 5000, 5);
                        if (clientID == -1) {
                            System.out.println("Failed connecting to remote API server");
                            return;
                        }

                        double[][] xy = VrepService.CheckCleaningQuality(clientID, handles, 0F, r, sR);

                        XYChart.Series series = getSeries(xy);
                        XYChart.Series seriesI = getSeriesInter(xy,r);
                        lineChart.getData().add(series);
                        lineChart.getData().add(seriesI);

                        textForResult.setText(String.valueOf(RESULT));

                    } catch (NumberFormatException nfe) {
                        warning.setVisible(true);
                    }
                    finally {
                        VrepService.vrep.simxFinish(-1);
                    }
                }
            }
        });

        R.setLayoutX(10);
        R.setLayoutY(130);

        labelStart.setLayoutX(7);
        labelStart.setLayoutY(10);

        stepR.setLayoutX(10);
        stepR.setLayoutY(170);

        warning.setLayoutX(110);
        warning.setLayoutY(160);
        warning.setVisible(false);

        textR.setLayoutX(110);
        textR.setLayoutY(130);
        textR.setPrefWidth(100);

        textForStart.setLayoutX(110);
        textForStart.setLayoutY(10);
        textForStart.setPrefWidth(100);


        textForResult.setLayoutX(110);
        textForResult.setLayoutY(500);
        textForResult.setPrefWidth(100);
        labelResult.setLayoutX(10);
        labelResult.setLayoutY(500);


        textStepR.setLayoutX(110);
        textStepR.setLayoutY(170);
        textStepR.setPrefWidth(100);

        percents.setLayoutX(110);
        labelTest.setLayoutX(10);
        labelTest.setLayoutY(290);
        percents.setLayoutY(290);
        percents.setPrefWidth(100);

        root.getChildren().add(labelTest);
        root.getChildren().add(percents);
        root.getChildren().add(R);
        root.getChildren().add(textR);
        root.getChildren().add(warning);
        root.getChildren().add(labelStart);
        root.getChildren().add(labelResult);
        root.getChildren().add(textStepR);
        root.getChildren().add(textForResult);
        root.getChildren().add(stepR);
        root.getChildren().add(textForStart);
        /////////////////////////////////////////////////////////////////////////
        root.getChildren().add(lineChart);
        root.getChildren().add(btnPlot);
        root.getChildren().add(btnStart);
        root.getChildren().add(btnCalc);
        root.getChildren().add(btnTest);

        Scene s = new Scene(root, 1000, 600);

        primaryStage.setScene(s);
        primaryStage.show();
    }

    private  static XYChart.Series getSeries(double[][] xy)
    {
        XYChart.Series series = new XYChart.Series();
        series.setName("N(R)");

        if(xy.length <= 0)
            return null;

        for(int i=0;i<xy[0].length;i++) {
            series.getData().add(new XYChart.Data(xy[0][i], xy[1][i]));

        }

        return series;
    }
    private  static XYChart.Series getSeriesInter(double[][] xy,float r)
    {
        XYChart.Series series = new XYChart.Series();
        series.setName("approx N(R)");

        double[][] xyTable = new double[][]
                {
                        xy[0],
                        xy[1]
                };
        double[] ab=LeastSquares(xyTable, 2);
        //for(int i=0;i<xy[0].length;i++) {
        series.getData().add(new XYChart.Data(xy[0][0],ab[1]+ ab[0]* xy[0][0]));
        series.getData().add(new XYChart.Data(r,ab[1]+ ab[0]* r));
        //}

        RESULT = ab[0];

        return series;
    }
    private static double[][] makeSystem(double[][] xyTable, int basis){
        double[][] matrix = new double[basis][basis+1];
        for(int i=0; i<basis; i++){
            for(int j=0; j<basis; j++){
                matrix[i][j] = 0;
            }
        }
        for(int i=0; i<basis; i++){
            for(int j=0; j<basis; j++){
                double sumA=0, sumB = 0;
                for (int k = 0; k < xyTable[0].length; k++)
                {
                    sumA += Math.pow(xyTable[0][k], i) * Math.pow(xyTable[0][k], j);
                    sumB += xyTable[1][k] * Math.pow(xyTable[0][k], i);
                }
                matrix[i][j] = sumA;
                matrix[i][basis] = sumB;
            }
        }
        return matrix;
    }

    private static double[] Gauss(int rowCount, int colCount){
        int i;
        mask = new int[colCount - 1];
        for (i = 0; i < colCount - 1; i++)
            mask[i] = i;
        if (GaussDirectPass(rowCount, colCount))
        {
            double[] answer = GaussReversePass(colCount, rowCount);
            return answer;
        }
        else return null;
    }

    private static boolean GaussDirectPass(int rowCount, int colCount){
        int i, j, k, maxId, tmpInt;
        double maxVal, tmpDouble;
        for (i = 0; i < rowCount; i++)
        {
            maxId = i;
            maxVal = matrix[i][i];
            for (j = i + 1; j < colCount - 1; j++)
                if(Math.abs(maxVal)<Math.abs(matrix[i][j]))
                {
                    maxVal = matrix[i][j];
                    maxId = j;
                }
            if (maxVal == 0) return false;
            if (i != maxId)
            {
                for (j = 0; j < rowCount; j++)
                {
                    tmpDouble = matrix[j][i];
                    matrix[j][i] = matrix[j][maxId];
                    matrix[j][maxId] = tmpDouble;
                }
                tmpInt = mask[i];
                mask[i] = mask[maxId];
                mask[maxId] = tmpInt;
            }
            for (j = 0; j < colCount; j++) matrix[i][j] /= maxVal;
            for (j = i + 1; j < rowCount; j++)
            {
                double tempMn = matrix[j][i];
                for (k = 0; k < colCount; k++)
                    matrix[j][k] -= matrix[i][k] * tempMn;
            }
        }
        return true;
    }

    private static double[] GaussReversePass(int colCount, int rowCount){
        int i,j,k;
        for (i = rowCount - 1; i >= 0; i--)
            for (j = i - 1; j >= 0; j--)
            {
                double tempMn = matrix[j][i];
                for (k = 0; k < colCount; k++)
                    matrix[j][k] -= matrix[i][k] * tempMn;
            }
        double[] answer = new double[rowCount];
        for (i = 0; i < rowCount; i++) answer[mask[i]] = matrix[i][colCount - 1];
        return answer;
    }

    private static double[] LeastSquares(double[][] xyTable, int basis){
        matrix = makeSystem(xyTable, basis);
        for(int i=0; i<basis; i++)
            for(int j=0; j<basis; j++)
                System.out.println(matrix[i][j]);
        double[] result = Gauss(basis, basis + 1);
        String str = "";
        double[] ab =new  double[2];
        ab[0] = result[basis-1];
        ab[1] = result[0];
        return ab;
    }
    public static void main(String[] args) {
        System.loadLibrary("remoteApiJava");
        launch(args);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
}
