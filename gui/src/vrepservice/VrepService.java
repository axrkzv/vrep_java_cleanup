package vrepservice;

import coppelia.IntW;
import coppelia.IntWA;
import coppelia.FloatWA;
import coppelia.remoteApi;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Stream;

/**
 * Created by asus on 12/27/2015.
 */
public class VrepService {
    public static remoteApi vrep = new remoteApi();
    private static final String leftUpperBorderCoordsObjName = "LeftUpperBorderCoords";
    private static final String rightBottomBorderCoordsObjName = "RightBottomBorderCoords";
    private static final String centerCoordsObjName = "CenterCoords";

    private static boolean IsErrorCodeOk(int errorCode)
    {
        if (errorCode == vrep.simx_return_ok)
            return true;

        System.out.format("error code: %d\n", errorCode);
        return false;
    }

    private static boolean IsPointInCircle(float x0, float y0, float r, float[] point)
    {
        if ( Math.pow(x0 - point[0], 2) + Math.pow(y0 - point[1], 2) <= Math.pow(r, 2))
            return true;
        return false;
    }

    private static float[] GetBounds(int clientID)
    {
        IntW leftUpperBorderCoordsHandleIntW = new IntW(1);
        IntW rightBottomBorderCoordsHandleIntW = new IntW(1);
        IntW centerCoordsHandleIntW = new IntW(1);

        if (    !IsErrorCodeOk(vrep.simxGetObjectHandle(clientID, leftUpperBorderCoordsObjName, leftUpperBorderCoordsHandleIntW, vrep.simx_opmode_oneshot_wait)) ||
                !IsErrorCodeOk(vrep.simxGetObjectHandle(clientID, rightBottomBorderCoordsObjName, rightBottomBorderCoordsHandleIntW, vrep.simx_opmode_oneshot_wait)) ||
                !IsErrorCodeOk(vrep.simxGetObjectHandle(clientID, centerCoordsObjName, centerCoordsHandleIntW, vrep.simx_opmode_oneshot_wait)))
        {
            return null;
        }

        FloatWA leftUpperBorderCoordsFloatWA = new FloatWA(3);
        FloatWA rightBottomBorderCoordsFloatWA = new FloatWA(3);
        FloatWA centerCoordsFloatWA = new FloatWA(3);
        if (    !IsErrorCodeOk(vrep.simxGetObjectPosition(clientID, leftUpperBorderCoordsHandleIntW.getValue(), -1, leftUpperBorderCoordsFloatWA, vrep.simx_opmode_oneshot_wait)) ||
                !IsErrorCodeOk(vrep.simxGetObjectPosition(clientID, rightBottomBorderCoordsHandleIntW.getValue(), -1, rightBottomBorderCoordsFloatWA, vrep.simx_opmode_oneshot_wait)) ||
                !IsErrorCodeOk(vrep.simxGetObjectPosition(clientID, centerCoordsHandleIntW.getValue(), -1, centerCoordsFloatWA, vrep.simx_opmode_oneshot_wait)))
        {
            return null;
        }

        float[] leftUpperBorderCoords = leftUpperBorderCoordsFloatWA.getArray();
        float[] rightBottomBorderCoords = rightBottomBorderCoordsFloatWA.getArray();

        float cx = centerCoordsFloatWA.getArray()[0];
        float cy = centerCoordsFloatWA.getArray()[1];

        float minX;
        float minY;
        float maxX;
        float maxY;

        if(leftUpperBorderCoords[0] < rightBottomBorderCoords[0])
        {
            minX = leftUpperBorderCoords[0];
            maxX = rightBottomBorderCoords[0];
        }
        else
        {
            minX = rightBottomBorderCoords[0];
            maxX = leftUpperBorderCoords[0];
        }

        if(leftUpperBorderCoords[1] < rightBottomBorderCoords[1])
        {
            minY = leftUpperBorderCoords[1];
            maxY = rightBottomBorderCoords[1];
        }
        else
        {
            minY = rightBottomBorderCoords[1];
            maxY = leftUpperBorderCoords[1];
        }

        return new float[]
                {
                        minX, maxX, minY, maxY, cx, cy
                };
    }

    private static double[] toPrimitive(Double[] array)
    {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return new double[] {};
        }
        final double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i].doubleValue();
        }
        return result;
    }

    public static void CleanUp(int clientID, ArrayList<Integer> handles, int percentOfPurity)
    {
        int n = (int)(((double)percentOfPurity * handles.size()) / 100);
        if(n <= 0 || n > handles.size())
            return;

        System.out.println("n = " + n);

        float[] bounds = GetBounds(clientID);

        if(bounds == null)
            return;

        float minX = bounds[0];
        float maxX = bounds[1];
        float minY = bounds[2];
        float maxY = bounds[3];
        float cx = bounds[4];
        float cy = bounds[5];

        for(int i = 0; i < n; i++)
        {
            FloatWA rndPos = new FloatWA(3);
            float[] rndPosTmp = rndPos.getArray();
            rndPosTmp[0] = minX;
            rndPosTmp[1] = minY;
            rndPosTmp[2] = 0F;

            if(!IsErrorCodeOk(vrep.simxSetObjectPosition(clientID, handles.get(i), -1, rndPos, vrep.simx_opmode_oneshot_wait)))
                return;
        }
    }

    public static double[][] CheckCleaningQuality(int clientID, ArrayList<Integer> handles,
                                                  float minCleaningRadius,
                                                  float cleaningRadius,
                                                  float step)
    {
        float[][] objCoords = new float[handles.size()][];
        for(int i = 0; i < objCoords.length; i++)
        {
            FloatWA pos = new FloatWA(3);
            if(!IsErrorCodeOk(vrep.simxGetObjectPosition(clientID, handles.get(i), -1, pos, vrep.simx_opmode_oneshot_wait)))
                return null;

            objCoords[i] = pos.getArray();
        }

        float[] bounds = GetBounds(clientID);

        if(bounds == null)
            return null;

        float minX = bounds[0];
        float maxX = bounds[1];
        float minY = bounds[2];
        float maxY = bounds[3];
        float cx = bounds[4];
        float cy = bounds[5];

        ArrayList<Double> x = new ArrayList<Double>();
        ArrayList<Double> y = new ArrayList<Double>();
        for(float r = minCleaningRadius; r <= cleaningRadius; r += step)
        {
            int garbageCount = 0;
            for(int i = 0; i < objCoords.length; i++)
            {
                if(IsPointInCircle(cx, cy, r, objCoords[i]))
                    garbageCount++;
            }
            x.add((double)r);
            y.add((double)garbageCount);
        }

        double[][] xy = new double[][]
                {
                        Stream.of(x.toArray(new Double[x.size()])).mapToDouble(Double::doubleValue).toArray(),
                        Stream.of(y.toArray(new Double[y.size()])).mapToDouble(Double::doubleValue).toArray()
                };

        return xy;
    }

    public static ArrayList<Integer> GenerateGarbage(int clientID, int count)
    {
        IntW garbageExampleHandleIntW = new IntW(1);

        if (!IsErrorCodeOk(vrep.simxGetObjectHandle(clientID, "Garbage", garbageExampleHandleIntW, vrep.simx_opmode_oneshot_wait)))
            return null;

        float[] bounds = GetBounds(clientID);

        if(bounds == null)
            return null;

        float minX = bounds[0];
        float maxX = bounds[1];
        float minY = bounds[2];
        float maxY = bounds[3];
        float cx = bounds[4];
        float cy = bounds[5];

        int garbageExampleHandle = garbageExampleHandleIntW.getValue();
        IntWA objectHandlesToCopy = new IntWA(1);
        objectHandlesToCopy.getArray()[0] = garbageExampleHandle;

        ArrayList<Integer> handles = new ArrayList<Integer>();

        for(int i = 0; i < count; i++)
        {
            IntWA newObjectHandles = new IntWA(1);

            if(!IsErrorCodeOk(vrep.simxCopyPasteObjects(clientID, objectHandlesToCopy, newObjectHandles, vrep.simx_opmode_oneshot_wait)))
                return null;

            int handle = newObjectHandles.getArray()[0];
            handles.add(handle);

            Random rand = new Random();
            float x = rand.nextFloat() * (maxX - minX) + minX;
            float y = rand.nextFloat() * (maxX - minY) + minY;

            FloatWA rndPos = new FloatWA(3);
            float[] rndPosTmp = rndPos.getArray();
            rndPosTmp[0] = x;
            rndPosTmp[1] = y;
            rndPosTmp[2] = 0F;

            if(!IsErrorCodeOk(vrep.simxSetObjectPosition(clientID, handle, -1, rndPos, vrep.simx_opmode_oneshot_wait)))
                return null;
        }

        return handles;
    }
}
